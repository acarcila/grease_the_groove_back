import { Router } from "express";

export default interface IRestController {
    getRouter(): Router;
}