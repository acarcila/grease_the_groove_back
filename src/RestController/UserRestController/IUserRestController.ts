import express from 'express';
import IRestController from "../IRestController";

export default interface IUserRestController extends IRestController{
    findAll(request: express.Request, response: express.Response): void;
    findByQuery(request: express.Request, response: express.Response): void;
    create(request: express.Request, response: express.Response): void;
    update(request: express.Request, response: express.Response): void;
    delete(request: express.Request, response: express.Response): void;
}