import express, { Router } from "express";
import IUserServiceController from "../../ServiceController/UserServiceController/IUserServiceController";
import IUserRestController from "./IUserRestController";

export default class UserRestControllerImpl implements IUserRestController {
    private path: string;
    private router: Router;
    private userServiceController: IUserServiceController;

    constructor(
        path: string,
        router: Router,
        userServiceController: IUserServiceController
    ) {
        this.path = path;
        this.router = router;
        this.userServiceController = userServiceController;

        this.initializeRoutes();
    }

    public initializeRoutes = () => {
        this.router.get(this.path.concat("/findAll"), this.findAll);
        this.router.get(this.path.concat("/findByQuery"), this.findByQuery);
        this.router.post(this.path.concat("/create"), this.create);
        this.router.put(this.path.concat("/update"), this.update);
        this.router.delete(this.path.concat("/delete"), this.delete);
    };

    public getRouter = (): Router => {
        return this.router;
    };

    public findAll = (
        request: express.Request,
        response: express.Response
    ): void => {
        this.userServiceController.findAll(request, response);
    };

    public findByQuery = (
        request: express.Request,
        response: express.Response
    ): void => {
        this.userServiceController.findByQuery(request, response);
    };

    public create = (
        request: express.Request,
        response: express.Response
    ): void => {
        this.userServiceController.create(request, response);
    };

    public update = (
        request: express.Request,
        response: express.Response
    ): void => {
        this.userServiceController.update(request, response);
    };

    public delete = (
        request: express.Request,
        response: express.Response
    ): void => {
        this.userServiceController.delete(request, response);
    };
}
