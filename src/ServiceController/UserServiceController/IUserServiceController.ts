import express from "express";

export default interface IUserServiceController {
    findAll(request: express.Request, response: express.Response): void;
    findByQuery(request: express.Request, response: express.Response): void;
    create(request: express.Request, response: express.Response): void;
    update(request: express.Request, response: express.Response): void;
    delete(request: express.Request, response: express.Response): void;
}
