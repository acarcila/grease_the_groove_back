import express from "express";
import User from "../../Model/User";
import IUserServiceController from "./IUserServiceController";

export default class UserServiceControllerImpl
    implements IUserServiceController {
    constructor() {}

    findAll = (request: express.Request, response: express.Response): void => {
        response.json("findAll");
    };

    findByQuery = (
        request: express.Request,
        response: express.Response
    ): void => {
        response.json("findByQuery");
    };

    create = (request: express.Request, response: express.Response): void => {
        response.json("create");
    };

    update = (request: express.Request, response: express.Response): void => {
        response.json("update");
    };

    delete = (request: express.Request, response: express.Response): void => {
        response.json("delete");
    };
}
