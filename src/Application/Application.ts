import express from "express";
import IRestController from "../RestController/IRestController";

export default class Application {
    private application: express.Application;
    private port: number;
    constructor(controllers: Array<IRestController>, port: number) {
        this.application = express();
        this.port = port;

        this.initializeMiddlewares();
        this.initializeControllers(controllers);
    }

    private initializeMiddlewares = () => {
        this.application.use(express.json());
    };

    private initializeControllers = (controllers: Array<IRestController>) => {
        controllers.forEach((controller) =>
            this.application.use("/", controller.getRouter())
        );
    };

    public start = () => {
        this.application.listen(this.port, () =>
            console.log(`Application is listening in port ${this.port}`)
        );
    };
}
