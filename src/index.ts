import express, { Router } from "express";
import Application from "./Application/Application";
import IRestController from "./RestController/IRestController";
import UserRestControllerImpl from "./RestController/UserRestController/UserRestControllerImpl";
import UserServiceControllerImpl from "./ServiceController/UserServiceController/UserServiceControllerImpl";

class Main {
    private application: Application;

    constructor(port: number) {
        const router: Router = express.Router();

        const controllers: Array<IRestController> = [
            new UserRestControllerImpl(
                "/user",
                router,
                new UserServiceControllerImpl()
            ),
        ];
        this.application = new Application(controllers, port);
    }

    public start = () => {
        this.application.start();
    };
}

(() => new Main(5000).start())();
