export default class User {
    private _id: number;
    private _username: string;
    private _name: string;
    private _email: string;
    private _birthday: number;

    constructor(
        id?: number,
        username?: string,
        name?: string,
        email?: string,
        birthday?: number
    ) {
        this._id = id || 0;
        this._username = username || "";
        this._name = name || "";
        this._email = email || "";
        this._birthday = birthday || 0;
    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
    public get username(): string {
        return this._username;
    }
    public set username(value: string) {
        this._username = value;
    }
    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }
    public get email(): string {
        return this._email;
    }
    public set email(value: string) {
        this._email = value;
    }
    public get birthday(): number {
        return this._birthday;
    }
    public set birthday(value: number) {
        this._birthday = value;
    }
}
